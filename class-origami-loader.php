<?php
/**
 * Plugin Name:     Origami
 * Plugin URI:      https://widgitlabs.com
 * Description:     A library for implementing a simple, transparent settings panel in WordPress.
 * Author:          Widgit Team
 * Author URI:      https://widgitlabs.com
 * Version:         1.0.0
 * Text Domain:     origami
 * Domain Path:     languages
 *
 * @package         WidgitLabs\Origami\Bootstrap
 * @author          Daniel J Griffiths <dgriffiths@evertiro.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// Conditionally load the library.
if ( ! class_exists( 'Origami' ) ) {
	$base_dir = trailingslashit( dirname( __FILE__ ) );

	if ( file_exists( $base_dir . 'class-origami.php' ) ) {
		require_once $base_dir . 'class-origami.php';
	} else {
		$files = glob( $base_dir . '**/class-origami.php' );

		if ( 1 === count( $files ) ) {
			require_once $files[0];
		}
	}
}


// Conditionally load the demo.
if ( ! class_exists( 'Origami_Demo' ) && origami_get_demo_mode_status() ) {
	$base_dir = trailingslashit( dirname( __FILE__ ) );
	$files    = glob( $base_dir . '**/class-origami-demo.php' );

	if ( 1 === count( $files ) ) {
		require_once $files[0];
	}
}


/**
 * Add plugin row meta.
 *
 * @since       1.0.0
 * @param       array  $row_meta The current plugin meta row.
 * @param       string $plugin_file The plugin file.
 * @return      array  $row_meta The updated plugin meta row
 */
function origami_plugin_row_meta( $row_meta, $plugin_file ) {
	if ( basename( __FILE__ ) === basename( $plugin_file ) && current_user_can( 'update_plugins' ) ) {

		$link_url   = wp_nonce_url( add_query_arg( array( 'origami-action' => 'toggle_demo_mode' ) ) );
		$link_label = ( origami_get_demo_mode_status() ) ? __( 'Disable Demo Mode', 'origami' ) : __( 'Enable Demo Mode', 'origami' );


		$row_meta[] = sprintf( '<a href="%s">%s</a>', $link_url, $link_label );
	}

	return $row_meta;
}
add_filter( 'plugin_row_meta', 'origami_plugin_row_meta', 10, 2 );


/**
 * Get the current status of demo mode.
 *
 * @since       1.0.0
 * @return      bool $enabled True if enabled, false otherwise
 */
function origami_get_demo_mode_status() {
	if ( is_network_admin() ) {
		return (bool) get_network_option( null, 'origami_demo_mode' );
	}

	return (bool) get_option( 'origami_demo_mode' );
}


/**
 * Toggle the status of demo mode.
 *
 * @since       1.0.0
 * @return      bool $enabled True if enabled, false otherwise
 */
function origami_toggle_demo_mode_status() {
	$enabled = origami_get_demo_mode_status();

	if ( is_network_admin() ) {
		return update_network_option( null, 'origami_demo_mode', ! $enabled );
	}

	return update_option( 'origami_demo_mode', ! $enabled );
}


/**
 * Display notice on demo toggle
 *
 * @since       1.0.0
 * @return      void
 */
function origami_demo_mode_notice() {
	if ( isset( $_REQUEST['origami_nonce'] ) ) {
		check_admin_referer( 'origami_nonce', 'origami_nonce' );
	}

	$get = wp_unslash( $_GET );

	if ( isset( $get['updated'] ) && 'origami-demo-mode' === $get['updated'] ) {
		$status = ( origami_get_demo_mode_status() ) ? __( 'Origami demo mode enabled.', 'origami' ) : __( 'Origami demo mode disabled.', 'origami' );

		echo '<div id="message" class="updated notice is-dismissible"><p>' . esc_html( $status ) . '</p></div>';
	}
}
add_action( 'admin_head', 'origami_demo_mode_notice' );


/**
 * Toggle demo mode
 *
 * @since       1.0.0
 * @return      void
 */
function origami_toggle_demo_mode() {
	origami_toggle_demo_mode_status();
	wp_safe_redirect(
		add_query_arg(
			array(
				'updated'        => 'origami-demo-mode',
				'origami-action' => false,
				'_wpnonce'       => false,
			)
		)
	);
	exit;
}
add_action( 'origami_toggle_demo_mode', 'origami_toggle_demo_mode' );


/**
 * Processes all actions sent via POST and GET by looking for the 'origami-action'
 * request and running do_action() to call the function
 *
 * @since       1.0.0
 * @return      void
 */
function origami_process_actions() {
	if ( isset( $_REQUEST['origami_nonce'] ) ) {
		check_admin_referer( 'origami_nonce', 'origami_nonce' );
	}

	$post = wp_unslash( $_POST );

	if ( isset( $post['origami-action'] ) ) {
		do_action( 'origami_' . $post['origami-action'], $post );
	}

	$get = wp_unslash( $_GET );

	if ( isset( $get['origami-action'] ) ) {
		do_action( 'origami_' . $get['origami-action'], $get );
	}
}
add_action( 'admin_init', 'origami_process_actions' );
