# Changelog

## [Unreleased]
### Added
- Support for optgroups in select fields.
- New `get_css_value` public helper function.

### Changed
- Rebranded as Origami
- Improved tooltip implementation.

## [2.0.0] - 22 Feb, 2021
### Added
- Support for network level settings in WordPress Multisite.
- Support for editor type args for editor fields.
- Support for std arg for checkboxes.
- Support for std arg for multicheck fields.
- Reset section option.
- Unsavable sections per tab.

### Changed
- Complete codebase refactor.
- Improved CodeMirror integration.
- Improved Select2 integration.
- Improved save handling for multicheck fields.
- Improved redirect URL cleanup.
- Improved CI suite.

### Fixed
- Broken upload field when multiple panels exist.

## [1.4.0] - 25 Nov, 2020
### Added
- Sysinfo module host platform information.

### Changed
- Improved asset path detection.
- Improved the sysinfo module plugin data sections.
- Rewrote module loader codebase.

## [1.3.0] - 04 Apr, 2020
### Changed
- Rewrote most of how modules are handled behind the scenes.

## [1.2.6] - 28 May, 2020
### Changed
- Improved Select2 handling.

## [1.2.5] - 25 May, 2020
### Changed
- Improvements to the CI suite.
- More code cleanup.

## [1.2.4] - 25 May, 2020
### Changed
- Overall optimization to the codebase.

### Fixed
- Incorrect textdomains in the licensing module.
- Typo in a CSS class name.

## [1.2.3] - 25 May, 2020
### Security
- Fixed a broken nonce.

## [1.2.2] - 25 May, 2020
### Security
- Actually actual WPCS coding standards.

## [1.2.1] - 13 Feb, 2020
### Security
- Added permissions check for sysinfo download action.

## [1.2.0] - 06 Feb, 2020
### Security
- First pass at bringing the codebase up to WPCS coding standards.

## [1.0.1] - 14 May, 2019
### Added
- Filter to allow extending whitelisted settings arguments.

### Fixed
- Accidental regression that prevented saving text fields on multiple tabs.

## [1.0.0] - 10 May, 2019
### Added
- First official release!
