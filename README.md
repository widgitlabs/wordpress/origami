# Origami

[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)](https://gitlab.com/widgitlabs/wordpress/origami/blob/master/license.txt)
[![Pipelines](https://gitlab.com/widgitlabs/wordpress/origami/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/wordpress/origami/pipelines)
[![Code Quality](https://app.codacy.com/project/badge/Grade/0feb46eef67b432fb171904a80a085cf)](https://app.codacy.com/gl/widgitlabs/origami)
[![Packagist](https://img.shields.io/packagist/v/widgitlabs/origami.svg)](https://packagist.org/packages/widgitlabs/origami)
[![Packagist](https://img.shields.io/packagist/dt/widgitlabs/origami.svg)](https://packagist.org/packages/widgitlabs/origami)
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)](https://discord.gg/jrydFBP)

## What Is This

Depending on the size of a project, you may be able to get away with adding
settings to an existing WordPress page (or the customizer). On the other end
of the spectrum, you may need (or want) to implement a full-scale control panel
like Redux. However, what if your project is somewhere in the middle?
Alternatively, what if you need a control panel but don't want the bulk that
goes along with most? This need is something that I've struggled with for some
time. Origami is a simple, standards-compliant library for creating
theme and plugin settings panels with a difference. Built on the core of the
excellent system used by Easy Digital Downloads, Origami strives to be
simple, extensible, and maintain a native look and feel.

_**Note:** The license field is horribly outdated and should not be used in
production without extensive testing until I have access to SL again and can
properly update it._

More information can be found on the [wiki](https://gitlab.com/widgitlabs/wordpress/origami/wikis/home).

## Installation

### Composer

Origami can be included in your project using [Composer](https://getcomposer.org/)
by adding it to your `composer.json`:

```json
{
    "require": {
        "widgitlabs/origami": "*"
    }
}
```

### Standalone

Origami can be included manually by downloading the latest release from
the [Releases page](https://gitlab.com/widgitlabs/wordpress/origami/-/releases)
and extracting it in your project directory.

## Demo Mode

Origami installations only include the actual settings library, but for
testing and development use we also provide a full WordPress plugin including a
"Demo Mode" which can be activated through the WordPress Plugins page. This
option is currently _only_ available through GitLab.

### Prebuilt Package

For every release we provide a prebuilt version of the WordPress plugin, which
can be downloaded through the [GitLab Package Registry](https://gitlab.com/widgitlabs/wordpress/origami/-/packages).

### Git

```sh
$ git clone https://gitlab.com/widgitlabs/wordpress/origami.git

# or

$ git submodule add https://gitlab.com/widgitlabs/wordpress/origami.git
```

## Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/wordpress/origami/issues)!

## Contributions

Anyone is welcome to contribute to the library. Please read the
[guidelines for contributing](https://github.com/widgitlabs/wordpress/origami/blob/master/CONTRIBUTING.md)
to this repository.

There are various ways you can contribute:

1. Raise an [Issue](https://gitlab.com/widgitlabs/wordpress/origami/issues)
   on GitLab
2. Send us a Pull Request with your bug fixes and/or new features
3. Provide feedback and suggestions on [enhancements](https://gitlab.com/widgitlabs/wordpress/origami/issues?label_name[]=Enhancement)
4. Help [translate](https://poeditor.com/join/project?hash=klXrYxkszL)
   Origami
