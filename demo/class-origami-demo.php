<?php
/**
 * Origami Demo
 *
 * @package         WidgitLabs\Origami\Demo
 * @since           1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Origami_Demo' ) ) {


	/**
	 * Main Origami_Demo class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	final class Origami_Demo {


		/**
		 * The one true Origami_Demo
		 *
		 * @access      private
		 * @since       1.0.0
		 * @var         Origami_Demo $instance The one true Origami_Demo
		 */
		private static $instance;


		/**
		 * The settings object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $settings The settings object
		 */
		public $settings;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @static
		 * @return      object self::$instance The one true Origami_Demo
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Origami_Demo ) ) {
				self::$instance = new Origami_Demo();
				self::$instance->setup_constants();
				self::$instance->includes();
				self::$instance->hooks();
			}

			return self::$instance;
		}


		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'origami' ), '1.0.0' );
		}


		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'origami' ), '1.0.0' );
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin path.
			if ( ! defined( 'ORIGAMI_DIR' ) ) {
				$path = str_replace( 'demo/', '', plugin_dir_path( __FILE__ ) );
				$path = trailingslashit( substr( $path, 0, -1 ) );

				define( 'ORIGAMI_DIR', $path );
			}

			// Plugin URL.
			if ( ! defined( 'ORIGAMI_URL' ) ) {
				$path = trailingslashit( str_replace( 'demo/', '', plugin_dir_url( __FILE__ ) ) );

				define( 'ORIGAMI_URL', $path );
			}
		}


		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}


		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			global $origami_demo_options;

			require_once ORIGAMI_DIR . 'demo/register-settings.php';

			$location = is_network_admin() ? 'network' : '';

			self::$instance->settings     = new Origami( 'origami-demo', 'core', array( 'sysinfo' ), $location );
			$origami_demo_options = self::$instance->settings->get_settings();
		}


		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'origami_languages_directory', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'origami' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'origami', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/origami/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/origami/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/origami folder.
				load_textdomain( 'origami', $mofile_global );
				return;
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/origami/languages/ folder.
				load_textdomain( 'origami', $mofile_local );
				return;
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/origami/ folder.
				load_textdomain( 'origami', $mofile_core );
				return;
			}

			// Load the default language files.
			load_plugin_textdomain( 'origami', false, $lang_dir );
		}
	}
}


/**
 * The main function responsible for returning the one true Origami_Demo
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $Origami_Demo = origami_demo(); ?>
 *
 * @since       1.0.0
 * @return      Origami_Demo The one true Origami_Demo
 */
function origami_demo() {
	return Origami_Demo::instance();
}

// Get things started.
origami_demo();
