<?php
/**
 * Register settings
 *
 * @package     WidgitLabs\Origami\Demo\Admin\Settings\Register
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Setup the settings menu
 *
 * @since       1.0.0
 * @param       array $menu The default menu settings.
 * @return      array $menu Our defined settings
 */
function origami_demo_add_menu( $menu ) {
	$icon = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZ' .
			'z0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAy' .
			'Ni41LjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ' .
			'1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG' .
			'1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0ia' .
			'HR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJ' .
			'IHZpZXdCb3g9IjAgMCAxNzAgMTcwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ' .
			'6bmV3IDAgMCAxNzAgMTcwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8cGF0aC' .
			'BpZD0iWE1MSURfMTFfIiBkPSJNMTYuOCwxMTIuOGw2OC4xLDU0LjJMMTUzLDExO' .
			'WwwLjIsMC4yVjIuOUw4NC40LDYwLjNMMTYuOCwyLjlWMTEyLjh6IE0xNDIuOCwx' .
			'MDkuOUw5My4yLDY3LjhsNDkuNi00My45DQoJVjEwOS45eiBNMjUuNCwyNC43bDE' .
			'xMC41LDkzLjVsLTI5LjgsMjEuMUg2Ni45bC0zOS42LTMxLjVMMjUuNCwyNC43ei' .
			'IvPg0KPC9zdmc+DQo=';

	$menu['type']        = 'menu';                     // Can be set to 'submenu' or 'menu'. Defaults to 'menu'.
	$menu['parent']      = null;                       // If 'type' is set to 'submenu', defines the parent menu to place our menu under. Defaults to 'options-general.php'.
	$menu['page_title']  = __( 'Origami', 'origami' ); // The page title. Defaults to 'Origami'.
	$menu['show_title']  = true;                       // Whether or not to display the title at the top of the page.
	$menu['menu_title']  = __( 'Origami', 'origami' ); // The menu title. Defaults to 'Origami'.
	$menu['capability']  = 'manage_options';           // The minimum capability required to access the settings panel. Defaults to 'manage_options'.
	$menu['icon']        = $icon;                      // An (optional) icon for your menu item. Follows the same standards as the add_menu_page() function in WordPress.
	$menu['position']    = null;                       // Where in the menu to display our new menu. Defaults to 'null' (bottom of the menu).
	$menu['allow_reset'] = true;                       // Whether or not to allow users to reset the settings per-section.

	return $menu;
}
add_filter( 'origami_demo_menu', 'origami_demo_add_menu' );


/**
 * Define our settings tabs
 *
 * @since       1.0.0
 * @param       array $tabs The default tabs.
 * @return      array $tabs Our defined tabs
 */
function origami_demo_settings_tabs( $tabs ) {
	$tabs['core']    = __( 'Core', 'origami' );
	$tabs['support'] = __( 'Support', 'origami' );

	return $tabs;
}
add_filter( 'origami_demo_settings_tabs', 'origami_demo_settings_tabs' );


/**
 * Define settings sections
 *
 * @since       1.0.0
 * @param       array $sections The default sections.
 * @return      array $sections Our defined sections
 */
function origami_demo_registered_settings_sections( $sections ) {
	$sections = array(
		'core'    => array(
			'main'            => __( 'Intro', 'origami' ),
			'checkbox_fields' => __( 'Checkbox/Radio Fields', 'origami' ),
			'color_fields'    => __( 'Color Fields', 'origami' ),
			'editor_fields'   => __( 'Editor Fields', 'origami' ),
			'html_fields'     => __( 'HTML Fields', 'origami' ),
			'select_fields'   => __( 'Select Fields', 'origami' ),
			'text_fields'     => __( 'Text Fields', 'origami' ),
			'upload_fields'   => __( 'Upload Fields', 'origami' ),
		),
		'support' => array(),
	);

	return $sections;
}
add_filter( 'origami_demo_registered_settings_sections', 'origami_demo_registered_settings_sections' );


/**
 * Disable save button on unsavable tabs
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function origami_demo_define_unsavable_tabs() {
	$tabs = array( 'support' );

	return $tabs;
}
add_filter( 'origami_demo_unsavable_tabs', 'origami_demo_define_unsavable_tabs' );


/**
 * Disable save button on unsavable sections
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function origami_demo_define_unsavable_sections() {
	$sections = array( 'core/main' );

	return $sections;
}
add_filter( 'origami_demo_unsavable_sections', 'origami_demo_define_unsavable_sections' );


/**
 * Define our settings
 *
 * @since       1.0.0
 * @param       array $settings The default settings.
 * @return      array $settings Our defined settings
 */
function origami_demo_registered_settings( $settings ) {
	$new_settings = array(
		'core'    => array(
			'main'            => array(
				array(
					'id'            => 'field_header',
					'name'          => '<h2>' . __( 'Welcome!', 'origami' ) . '</h2>',
					'desc'          => '',
					'type'          => 'header',
					'tooltip_title' => __( 'Tip', 'origami' ),
					'tooltip_desc'  => __( 'This is a header field, and can be used with or without a tooltip.', 'origami' ),
				),
				array(
					'id'   => 'core_intro',
					'name' => __( 'What is Origami?', 'origami' ),
					'desc' => '',
					'type' => 'hook',
				),
				array(
					'id'            => 'field_descriptive_text',
					'name'          => __( 'Descriptive Text Field', 'origami' ),
					'desc'          => __( 'This is a descriptive text field.', 'origami' ),
					'type'          => 'descriptive_text',
					'tooltip_title' => __( 'Tip', 'origami' ),
					'tooltip_desc'  => __( 'This is a descriptive_text field, and can be used with or without a tooltip.', 'origami' ),
				),
			),
			'checkbox_fields' => array(
				array(
					'id'            => 'field_checkbox',
					'name'          => __( 'Checkbox Field', 'origami' ),
					'desc'          => __( 'This is a checkbox.', 'origami' ),
					'type'          => 'checkbox',
					'class'         => '',
					'std'           => 'checked',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_multicheck',
					'name'          => __( 'Multicheck Field', 'origami' ),
					'desc'          => __( 'This is a multicheck field.', 'origami' ),
					'type'          => 'multicheck',
					'class'         => '',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
					'options'       => array(
						'option1' => __( 'Option 1', 'origami' ),
						'option2' => __( 'Option 2', 'origami' ),
						'option3' => __( 'Option 3', 'origami' ),
					),
					'std'           => array(
						'option2' => 'checked',
					),
				),
				array(
					'id'            => 'field_radio',
					'name'          => __( 'Radio Field', 'origami' ),
					'desc'          => __( 'This is a radio field.', 'origami' ),
					'type'          => 'radio',
					'class'         => '',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
					'options'       => array(
						'option1' => __( 'Option 1', 'origami' ),
						'option2' => __( 'Option 2', 'origami' ),
						'option3' => __( 'Option 3', 'origami' ),
					),
					'std'           => 'option2',
				),
			),
			'color_fields'    => array(
				array(
					'id'            => 'field_color',
					'name'          => __( 'Color Field', 'origami' ),
					'desc'          => __( 'This is a color field.', 'origami' ),
					'type'          => 'color',
					'class'         => '',
					'std'           => '#1793d1',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
			),
			'editor_fields'   => array(
				array(
					'id'             => 'field_editor',
					'name'           => __( 'Editor Field', 'origami' ),
					'desc'           => __( 'This is an editor field.', 'origami' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is a "standard" editor field.', 'origami' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => true,
					'teeny'          => false,
					'editor_tinymce' => true,
					'editor_html'    => true,
					'default_editor' => '',
					'tooltip_title'  => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'             => 'field_editor_no_buttons',
					'name'           => __( 'Editor Field (No Buttons)', 'origami' ),
					'desc'           => __( 'This is an editor field.', 'origami' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is an editor field without buttons.', 'origami' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => false,
					'teeny'          => false,
					'editor_tinymce' => true,
					'editor_html'    => true,
					'default_editor' => '',
					'tooltip_title'  => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'             => 'field_editor_tinymce_only',
					'name'           => __( 'Editor Field (TinyMCE Only)', 'origami' ),
					'desc'           => __( 'This is an editor field.', 'origami' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is an editor field with html disabled.', 'origami' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => true,
					'teeny'          => true,
					'editor_tinymce' => true,
					'editor_html'    => false,
					'default_editor' => '',
					'tooltip_title'  => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'             => 'field_editor_html_only',
					'name'           => __( 'Editor Field (HTML Only)', 'origami' ),
					'desc'           => __( 'This is an editor field.', 'origami' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is an editor field with TinyMCE disabled.', 'origami' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => true,
					'teeny'          => true,
					'editor_tinymce' => false,
					'editor_html'    => true,
					'default_editor' => '',
					'tooltip_title'  => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'             => 'field_editor_html_default',
					'name'           => __( 'Editor Field (HTML Default)', 'origami' ),
					'desc'           => __( 'This is an editor field.', 'origami' ),
					'type'           => 'editor',
					'class'          => '',
					'std'            => __( 'This is an editor field with quicktags selected by default.', 'origami' ),
					'allow_blank'    => true,
					'size'           => 10,
					'wpautop'        => true,
					'buttons'        => true,
					'teeny'          => true,
					'editor_tinymce' => true,
					'editor_html'    => true,
					'default_editor' => 'html',
					'tooltip_title'  => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'   => __( 'Tooltip description.', 'origami' ),
				),
			),
			'html_fields'     => array(
				array(
					'id'            => 'field_html',
					'name'          => __( 'HTML Field', 'origami' ),
					'desc'          => __( 'This is an HTML (CodeMirror) field.', 'origami' ),
					'type'          => 'html',
					'std'           => '',
					'mode'          => 'text/x-php',
					'line_numbers'  => true,
					'indent_size'   => 2,
					'indent_tabs'   => false,
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_html_no_line_numbers',
					'name'          => __( 'HTML Field (No Line Numbers)', 'origami' ),
					'desc'          => __( 'This is an HTML field with line numbers disabled.', 'origami' ),
					'type'          => 'html',
					'std'           => '',
					'mode'          => 'text/x-php',
					'line_numbers'  => false,
					'indent_size'   => 2,
					'indent_tabs'   => false,
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_html_custom_indent',
					'name'          => __( 'HTML Field (Custom Indentation)', 'origami' ),
					'desc'          => __( 'This is an HTML field with indent set to 4 spaces.', 'origami' ),
					'type'          => 'html',
					'std'           => '',
					'mode'          => 'text/x-php',
					'line_numbers'  => true,
					'indent_size'   => 4,
					'indent_tabs'   => false,
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
			),
			'select_fields'   => array(
				array(
					'id'            => 'field_select',
					'name'          => __( 'Select Field', 'origami' ),
					'desc'          => __( 'This is a select field.', 'origami' ),
					'type'          => 'select',
					'std'           => '',
					'placeholder'   => __( 'Pick something!', 'origami' ),
					'select2'       => false,
					'multiple'      => false,
					'options'       => array(
						'option1' => __( 'Option 1', 'origami' ),
						'option2' => __( 'Option 2', 'origami' ),
						'option3' => __( 'Option 3', 'origami' ),
					),
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_select_multi',
					'name'          => __( 'Select Field (Multiselect)', 'origami' ),
					'desc'          => __( 'This is a select field with multi-selection enabled.', 'origami' ),
					'type'          => 'select',
					'std'           => '',
					'placeholder'   => __( 'Pick something!', 'origami' ),
					'select2'       => false,
					'multiple'      => true,
					'options'       => array(
						'option1' => __( 'Option 1', 'origami' ),
						'option2' => __( 'Option 2', 'origami' ),
						'option3' => __( 'Option 3', 'origami' ),
					),
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_select_select2',
					'name'          => __( 'Select Field (Select2)', 'origami' ),
					'desc'          => __( 'This is a select field with Select2 enabled.', 'origami' ),
					'type'          => 'select',
					'std'           => '',
					'placeholder'   => __( 'Pick something!', 'origami' ),
					'select2'       => true,
					'multiple'      => false,
					'options'       => array(
						'option1' => __( 'Option 1', 'origami' ),
						'option2' => __( 'Option 2', 'origami' ),
						'option3' => __( 'Option 3', 'origami' ),
					),
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_select_select2_multi',
					'name'          => __( 'Select Field (Select2 Multiselect)', 'origami' ),
					'desc'          => __( 'This is a select field with Select2 and multi-selection enabled.', 'origami' ),
					'type'          => 'select',
					'std'           => '',
					'placeholder'   => __( 'Pick something!', 'origami' ),
					'select2'       => true,
					'multiple'      => true,
					'options'       => array(
						'option1' => __( 'Option 1', 'origami' ),
						'option2' => __( 'Option 2', 'origami' ),
						'option3' => __( 'Option 3', 'origami' ),
					),
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
			),
			'text_fields'     => array(
				array(
					'id'            => 'field_text',
					'name'          => __( 'Text Field', 'origami' ),
					'desc'          => __( 'This is a text field.', 'origami' ),
					'type'          => 'text',
					'std'           => '',
					'readonly'      => false,
					'disabled'      => false,
					'size'          => 'regular',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_text_read_only',
					'name'          => __( 'Text Field (Read Only)', 'origami' ),
					'desc'          => __( 'This is a read only text field.', 'origami' ),
					'type'          => 'text',
					'std'           => __( 'Don\'t edit me!', 'origami' ),
					'readonly'      => true,
					'disabled'      => false,
					'size'          => 'regular',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_text_disabled',
					'name'          => __( 'Text Field (Disabled)', 'origami' ),
					'desc'          => __( 'This is a large disabled text field.', 'origami' ),
					'type'          => 'text',
					'std'           => __( 'Don\'t edit me!', 'origami' ),
					'readonly'      => false,
					'disabled'      => true,
					'size'          => 'large',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_number',
					'name'          => __( 'Number Field', 'origami' ),
					'desc'          => __( 'This is a small number field.', 'origami' ),
					'type'          => 'number',
					'std'           => '0',
					'min'           => '0',
					'max'           => '999999',
					'step'          => '1',
					'size'          => 'small',
					'readonly'      => false,
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_number_step',
					'name'          => __( 'Number Field (Custom Step)', 'origami' ),
					'desc'          => __( 'This is a number field with step set to ".1" and custom min/max values.', 'origami' ),
					'type'          => 'number',
					'std'           => '0',
					'min'           => '-100',
					'max'           => '100',
					'step'          => '.1',
					'size'          => 'regular',
					'readonly'      => false,
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_number_read_only',
					'name'          => __( 'Number Field (Read Only)', 'origami' ),
					'desc'          => __( 'This is a read only number field.', 'origami' ),
					'type'          => 'number',
					'std'           => '0',
					'min'           => '0',
					'max'           => '999999',
					'step'          => '1',
					'size'          => 'small',
					'readonly'      => true,
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_textarea',
					'name'          => __( 'Textarea Field', 'origami' ),
					'desc'          => __( 'This is a text area field.', 'origami' ),
					'type'          => 'textarea',
					'std'           => '',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
				array(
					'id'            => 'field_password',
					'name'          => __( 'Password Field', 'origami' ),
					'desc'          => __( 'This is a password field.', 'origami' ),
					'type'          => 'password',
					'std'           => '',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
			),
			'upload_fields'   => array(
				array(
					'id'            => 'field_upload',
					'name'          => __( 'Upload Field', 'origami' ),
					'desc'          => __( 'This is an upload field.', 'origami' ),
					'type'          => 'upload',
					'std'           => '',
					'size'          => 'regular',
					'tooltip_title' => __( 'Tooltip Title', 'origami' ),
					'tooltip_desc'  => __( 'Tooltip description.', 'origami' ),
				),
			),
		),
		'support' => array(
			array(
				'id'   => 'support_header',
				'name' => __( 'Origami Support', 'origami' ),
				'desc' => '',
				'type' => 'header',
			),
			array(
				'id'   => 'system_info',
				'name' => __( 'System Info', 'origami' ),
				'desc' => '',
				'type' => 'sysinfo',
			),
		),
	);

	return array_merge( $settings, $new_settings );
}
add_filter( 'origami_demo_registered_settings', 'origami_demo_registered_settings' );


/**
 * Display our intro text (this uses the "hook" field!)
 *
 * @since       2.0.0
 * @return      void
 */
function origami_display_intro() {
	echo '<div style="max-width: 750px;">';
	echo wp_kses_post( __( 'Depending on the size of a project, you may be able to get away with adding settings to an existing WordPress page (or the customizer). On the other end of the spectrum, you may need (or want) to implement a full-scale control panel like <a href="https://reduxframework.com" target="_blank">Redux</a>. However, what if your project is somewhere in the middle? Alternatively, what if you need a control panel but don\'t want the bulk that goes along with most? This need is something that I\'ve struggled with for some time. Origami is a simple, standards-compliant library for creating theme and plugin settings panels with a difference. Built on the core of the excellent system used by <a href="https://easydigitaldownloads.com" target="_blank">Easy Digital Downloads</a>, Origami strives to be simple, extensible, and maintain a native look and feel.', 'origami' ) );
	echo '<br /><br />';
	echo wp_kses_post( __( 'Throughout the demo, we will be using tooltips to identify the individual fields shown. The above field is a <code>header</code> and this field is a <code>hook</code>. We will also be using the <code>descriptive_text</code> field type to show you how things are built.', 'origami' ) );
	echo '</div>';
}
add_action( 'origami_demo_core_intro', 'origami_display_intro' );
