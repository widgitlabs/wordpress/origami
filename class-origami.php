<?php
/**
 * Origami class
 *
 * @package     WidgitLabs\Origami
 * @since       1.0.0
 */

// phpcs:disable PHPCompatibility.Variables.ForbiddenThisUseContexts.Global, PHPCompatibility.Variables.ForbiddenGlobalVariableVariable.NonBareVariableFound

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Origami handler class
 *
 * @access      public
 * @since       1.0.0
 */
class Origami {


	/**
	 * The settings class version
	 *
	 * @var         string $version The settings class version
	 * @access      protected
	 * @since       1.0.0
	 */
	protected $version = '1.0.0';


	/**
	 * The page slug
	 *
	 * @var         string $slug The page slug
	 * @access      protected
	 * @since       1.0.0
	 */
	protected $slug;


	/**
	 * The page slug for names
	 *
	 * @var         string $func The page slug for names
	 * @access      protected
	 * @since       1.0.0
	 */
	protected $func;


	/**
	 * The default tab to display
	 *
	 * @var         string $default_tab The default tab to display
	 * @access      private
	 * @since       1.0.0
	 */
	private $default_tab;


	/**
	 * Whether or not to display the page title
	 *
	 * @var         bool $show_title Whether or not to display the page title
	 * @access      private
	 * @since       1.0.0
	 */
	private $show_title;


	/**
	 * The page title
	 *
	 * @var         bool page_title The page title
	 * @access      private
	 * @since       1.0.0
	 */
	private $page_title;


	/**
	 * Whether or not to allow resetting sections
	 *
	 * @var         bool Whether or not to allow resetting sections
	 * @access      private
	 * @since       1.0.0
	 */
	private $allow_reset;


	/**
	 * The modules object
	 *
	 * @var         object $modules The modules object
	 * @access      private
	 * @since       1.0.0
	 */
	private $modules = array();


	/**
	 * The WordPress admin area for this panel
	 *
	 * @var         string $location The WordPress admin area for this panel
	 * @access      private
	 * @since       1.0.0
	 */
	private $location;


	/**
	 * The helpers object
	 *
	 * @var         object $helpers The helpers object
	 * @access      public
	 * @since       1.0.0
	 */
	public $helpers;


	/**
	 * Get things started
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $slug The page slug.
	 * @param       string $default_tab The default settings tab to display.
	 * @param       array  $modules Any modules to enable.
	 * @param       array  $location The WordPress admin area for this panel.
	 * @return      void
	 */
	public function __construct( $slug = false, $default_tab = 'general', $modules = array(), $location = 'site' ) {
		// Bail if no slug is specified.
		if ( ! $slug ) {
			return;
		}

		// Setup variables.
		$this->slug        = $slug;
		$this->func        = str_replace( '-', '_', $slug );
		$this->default_tab = $default_tab;
		$this->modules     = $modules;
		$this->location    = $location;

		$this->hooks();
		$this->includes();
	}


	/**
	 * Run action and filter hooks
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function hooks() {
		// Add the setting page.
		if ( 'site' === $this->location ) {
			add_action( 'admin_menu', array( $this, 'add_settings_page' ), 10 );
		}

		// Add settings to the network admin page.
		if ( 'network' === $this->location ) {
			add_action( 'network_admin_menu', array( $this, 'add_settings_page' ), 10 );
		}

		// Register the settings.
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		add_filter( $this->func . '_settings_sanitize_text', array( $this, 'sanitize_text_field' ) );

		// Add styles and scripts.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ), 100 );

		// Process actions.
		add_action( 'admin_init', array( $this, 'process_actions' ) );

		// Handle tooltips.
		add_filter( $this->func . '_after_setting_output', array( $this, 'add_setting_tooltip' ), 10, 2 );

		// Handle section resets.
		add_action( $this->func . '_settings_reset_section', array( $this, 'reset_section' ), 10 );
	}


	/**
	 * Include any necessary files
	 *
	 * @access      private
	 * @since       1.0.0
	 * @return      void
	 */
	private function includes() {
		require_once trailingslashit( dirname( __FILE__ ) ) . 'includes/class-origami-helpers.php';

		$this->helpers = new Origami_Helpers( $this->slug, $this->func );

		$modules = is_array( $this->modules ) ? $this->modules : array();

		// Load enabled modules.
		if ( count( $modules ) > 0 ) {
			foreach ( $this->modules as $module ) {
				$class = 'Origami_' . str_replace( ' ', '_', ucwords( str_replace( '-', ' ', $module ) ) );

				if ( ! class_exists( $class ) ) {
					if ( file_exists( trailingslashit( dirname( __FILE__ ) ) . 'modules/' . $module . '/class-origami-' . $module . '.php' ) ) {
						require_once trailingslashit( dirname( __FILE__ ) ) . 'modules/' . $module . '/class-origami-' . $module . '.php';
					}
				}
			}
		}
	}


	/**
	 * Add settings pages
	 *
	 * @access      public
	 * @since       1.0.0
	 * @global      string ${this->func . '_settings_page'} The settings page slug
	 * @return      void
	 */
	public function add_settings_page() {
		$parent     = 'options-general.php';
		$capability = 'manage_options';

		// Setup things for network admin pages.
		if ( 'network' === $this->location ) {
			$parent     = 'settings.php';
			$capability = 'manage_network_options';
		}

		global $origami_pages;

		$menu = apply_filters(
			$this->func . '_menu',
			array(
				'type'        => 'menu',
				'parent'      => $parent,
				'page_title'  => __( 'Origami', 'origami' ),
				'show_title'  => false,
				'menu_title'  => __( 'Origami', 'origami' ),
				'capability'  => $capability,
				'icon'        => '',
				'position'    => null,
				'allow_reset' => true,
			)
		);

		$this->show_title  = $menu['show_title'];
		$this->page_title  = $menu['page_title'];
		$this->allow_reset = $menu['allow_reset'];

		if ( 'submenu' === $menu['type'] ) {
			$origami_pages[] = add_submenu_page( $menu['parent'], $menu['page_title'], $menu['menu_title'], $menu['capability'], $this->slug, array( $this, 'render_settings_page' ), $menu['position'] );
			return;
		}

		$origami_pages[] = add_menu_page( $menu['page_title'], $menu['menu_title'], $menu['capability'], $this->slug, array( $this, 'render_settings_page' ), $menu['icon'], $menu['position'] );
	}


	/**
	 * Render settings page
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function render_settings_page() {
		if ( isset( $_REQUEST[ $this->slug . '-nonce' ] ) ) {
			check_admin_referer( $this->slug . '-nonce', $this->slug . '-nonce' );
		}

		$get                 = wp_unslash( $_GET );
		$active_tab          = isset( $get['tab'] ) && array_key_exists( $get['tab'], $this->get_settings_tabs() ) ? $get['tab'] : $this->default_tab;
		$registered_sections = $this->get_settings_tab_sections( $active_tab );
		$sections            = $registered_sections;
		$key                 = 'main';

		if ( is_array( $sections ) ) {
			$key = key( $sections );
		}

		$active_section = isset( $get['section'] ) && ! empty( $registered_sections ) && array_key_exists( $get['section'], $registered_sections ) ? $get['section'] : $key;
		?>
		<div class="wrap origami-page">
			<?php
			if ( $this->show_title ) {
				echo '<h2>' . esc_html( $this->page_title ) . '</h2>';
			}

			// For some reason, the automatic display of the saved notice doesn't work on Multisite.
			if ( 'network' === $this->location && isset( $get['updated'] ) ) {
				echo '<div id="message" class="updated notice is-dismissible"><p>' . esc_html( __( 'Settings saved.', 'origami' ) ) . '</p></div>';
			}
			?>
			<h2 class="nav-tab-wrapper">
				<?php
				foreach ( $this->get_settings_tabs() as $tab_id => $tab_name ) {
					$tab_url = add_query_arg(
						array(
							'tab' => $tab_id,
						)
					);

					// Clean up tab URLs.
					$tab_url = remove_query_arg(
						array(
							'section',
							'updated',
							$this->slug . '-action',
							$this->slug . '-nonce',
							$this->slug . '-reset-nonce',
						),
						$tab_url
					);

					$active = $active_tab === $tab_id ? ' nav-tab-active' : '';

					echo '<a href="' . esc_url( $tab_url ) . '" title="' . esc_attr( $tab_name ) . '" class="nav-tab' . esc_attr( $active ) . '">' . esc_html( $tab_name ) . '</a>';
				}
				?>
			</h2>
			<?php
			$number_of_sections = count( (array) $sections );
			$number             = 0;

			if ( $number_of_sections > 1 ) {
				echo '<div><ul class="subsubsub">';

				foreach ( $sections as $section_id => $section_name ) {
					echo '<li>';

					$number++;
					$class   = '';
					$tab_url = add_query_arg(
						array(
							'settings' => false,
							'tab'      => $active_tab,
							'section'  => $section_id,
						)
					);

					$tab_url = remove_query_arg(
						array(
							'updated',
							$this->slug . '-action',
							$this->slug . '-nonce',
							$this->slug . '-reset-nonce',
						),
						$tab_url
					);

					if ( $active_section === $section_id ) {
						$class = 'current';
					}

					echo '<a class="' . esc_attr( $class ) . '" href="' . esc_url( $tab_url ) . '">' . esc_html( $section_name ) . '</a>';

					if ( $number !== $number_of_sections ) {
						echo '&nbsp;|&nbsp;';
					}

					echo '</li>';
				}

				echo '</ul></div>';
			}

			$submit_url = remove_query_arg(
				array(
					$this->slug . '-action',
					$this->slug . '-nonce',
					$this->slug . '-reset-nonce',
				)
			);
			?>
			<div id="tab_container" class="origami-options-table">
				<form method="post" action="<?php echo esc_url( $submit_url ); ?>" novalidate="novalidate">
					<table class="form-table">
						<?php
						settings_fields( $this->func . '_settings' );

						do_action( $this->func . '_settings_tab_top_' . $active_tab . '_' . $active_section );

						do_settings_sections( $this->func . '_settings_' . $active_tab . '_' . $active_section );

						do_action( $this->func . '_settings_tab_bottom_' . $active_tab . '_' . $active_section );
						?>
					</table>
					<?php
					$savable            = true;
					$unsavable_sections = apply_filters( $this->func . '_unsavable_sections', array() );

					if ( in_array( $active_tab, apply_filters( $this->func . '_unsavable_tabs', array() ), true ) ) {
						$savable = false;
					}

					if ( is_array( $unsavable_sections ) && ! empty( $unsavable_sections ) ) {
						foreach ( $unsavable_sections as $section ) {
							$section = explode( '/', $section );

							if ( count( $section ) === 1 ) {
								$tab     = $key;
								$section = $section[0];
							} elseif ( count( $section ) > 1 ) {
								$tab     = $section[0];
								$section = $section[1];
							}

							if ( $tab === $active_tab && $section === $active_section ) {
								$savable = false;
							}
						}
					}

					if ( $savable ) {
						wp_nonce_field( $this->slug . '-nonce', $this->slug . '-nonce' );
						echo '<p>';
						submit_button( __( 'Save Changes', 'origami' ), 'primary', 'submit', false );

						if ( $this->allow_reset ) {
							echo '&nbsp;&nbsp;';

							$reset_url = wp_nonce_url(
								add_query_arg(
									array(
										$this->slug . '-action' => 'reset_section',
									),
								),
								$this->slug . '-reset-nonce',
								$this->slug . '-reset-nonce'
							);

							printf( '<a class="button button-secondary" href="%s">%s</a>', esc_url( $reset_url ), esc_html( __( 'Reset Section', 'origami' ) ) );
						}

						echo '</p>';
					}
					?>
				</form>
			</div>
		</div>
		<?php
	}


	/**
	 * Retrieve the settings tabs
	 *
	 * @access      private
	 * @since       1.0.0
	 * @return      array $tabs The registered tabs for this panel
	 */
	private function get_settings_tabs() {
		return apply_filters( $this->func . '_settings_tabs', array() );
	}


	/**
	 * Retrieve settings tab sections
	 *
	 * @access      private
	 * @since       1.0.0
	 * @param       string $tab The current tab.
	 * @return      array $section The section items
	 */
	private function get_settings_tab_sections( $tab = false ) {
		$tabs     = false;
		$sections = $this->get_registered_settings_sections();

		if ( $tab && ! empty( $sections[ $tab ] ) ) {
			$tabs = $sections[ $tab ];
		} elseif ( $tab ) {
			$tabs = false;
		}

		return $tabs;
	}


	/**
	 * Retrieve the panel settings
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      array $settings The panel settings
	 */
	public function get_registered_settings() {
		return apply_filters( $this->func . '_registered_settings', array() );
	}


	/**
	 * Retrieve the panel settings sections
	 *
	 * @access      private
	 * @since       1.0.0
	 * @return      array $sections The registered sections
	 */
	private function get_registered_settings_sections() {
		global ${$this->func . '_sections'};

		if ( ! empty( ${$this->func . '_sections'} ) ) {
			return ${$this->func . '_sections'};
		}

		${$this->func . '_sections'} = apply_filters( $this->func . '_registered_settings_sections', array() );

		return ${$this->func . '_sections'};
	}


	/**
	 * Retrieve an option
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $key The key to retrieve.
	 * @param       mixed  $default The default value if key doesn't exist.
	 * @global      array ${$this->func . '_options'} The options array
	 * @return      mixed $value The value to return
	 */
	public function get_option( $key = '', $default = false ) {
		global ${$this->func . '_options'};

		$value = ! empty( ${$this->func . '_options'}[ $key ] ) ? ${$this->func . '_options'}[ $key ] : $default;
		$value = apply_filters( $this->func . '_get_option', $value, $key, $default );

		return apply_filters( $this->func . '_get_option_' . $key, $value, $key, $default );
	}


	/**
	 * Update an option
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $key The key to update.
	 * @param       mixed  $value The value to set key to.
	 * @return      bool true if updated, false otherwise
	 */
	public function update_option( $key = '', $value = false ) {
		// Bail if no key is set.
		if ( empty( $key ) ) {
			return false;
		}

		if ( empty( $value ) ) {
			$remove_option = $this->delete_option( $key );
			return $remove_option;
		}

		// Fetch a clean copy of the options array.
		$options = get_option( $this->func . '_settings' );

		if ( 'network' === $this->location ) {
			$options = get_network_option( null, $this->func . '_settings' );
		}

		// Allow devs to modify the value.
		$value = apply_filters( $this->func . '_update_option', $value, $key );

		// Try to update the option.
		$options[ $key ] = $value;

		if ( 'network' === $this->location ) {
			$did_update = update_network_option( null, $this->func . '_settings', $options );
		} elseif ( 'network' !== $this->location ) {
			$did_update = update_option( $this->func . '_settings', $options );
		}

		// Update the global.
		if ( $did_update ) {
			global ${$this->func . '_options'};
			${$this->func . '_options'}[ $key ] = $value;
		}

		return $did_update;
	}


	/**
	 * Delete an option
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $key The key to delete.
	 * @return      bool true if deleted, false otherwise
	 */
	public function delete_option( $key = '' ) {
		// Bail if no key is set.
		if ( empty( $key ) ) {
			return false;
		}

		global ${$this->func . '_options'};

		// Fetch a clean copy of the options array.
		$options = ( 'network' === $this->location ) ? get_network_option( null, $this->func . '_settings' ) : get_option( $this->func . '_settings' );

		// Try to unset the option.
		if ( isset( $options[ $key ] ) ) {
			unset( $options[ $key ] );
		}

		// Remove the option from the global.
		if ( isset( ${$this->func . '_options'}[ $key ] ) ) {
			unset( ${$this->func . '_options'}[ $key ] );
		}

		if ( 'network' === $this->location ) {
			$did_update = update_network_option( null, $this->func . '_settings', $options );
		} elseif ( 'network' !== $this->location ) {
			$did_update = update_option( $this->func . '_settings', $options );
		}

		// Update the global.
		if ( $did_update ) {
			${$this->func . '_options'} = $options;
		}

		return $did_update;
	}


	/**
	 * Retrieve all options
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      array $settings The options array
	 */
	public function get_settings() {
		$settings = ( 'network' === $this->location ) ? get_network_option( null, $this->func . '_settings' ) : get_option( $this->func . '_settings' );

		if ( ! $settings ) {
			$settings = array();

			if ( 'network' === $this->location ) {
				update_network_option( null, $this->func . '_settings', $settings );
			} elseif ( 'network' !== $this->location ) {
				update_option( $this->func . '_settings', $settings );
			}
		}

		return apply_filters( $this->func . '_get_settings', $settings );
	}


	/**
	 * Update all options
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $settings The new options array.
	 * @return      mixed void if updated, false otherwise
	 */
	public function update_options( $settings = array() ) {
		// Allow devs to modify the array.
		$settings = apply_filters( $this->func . '_update_options', $settings );

		if ( 'network' === $this->location ) {
			$did_update = update_network_option( null, $this->func . '_settings', $settings );
		} elseif ( 'network' !== $this->location ) {
			$did_update = update_option( $this->func . '_settings', $settings );
		}

		// Update the global.
		if ( $did_update ) {
			global ${$this->func . '_options'};
			${$this->func . '_options'} = $settings;
		}

		return $did_update;
	}


	/**
	 * Add settings sections and fields
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function register_settings() {
		if ( 'network' === $this->location && get_network_option( null, $this->func . '_settings' ) === false ) {
			add_network_option( null, $this->func . '_settings', array() );
		} elseif ( get_option( $this->func . '_settings' ) === false ) {
			add_option( $this->func . '_settings', array() );
		}

		foreach ( $this->get_registered_settings() as $tab => $sections ) {
			foreach ( $sections as $section => $settings ) {
				// Check for backwards compatibility.
				$section_tabs = $this->get_settings_tab_sections( $tab );

				if ( ! is_array( $section_tabs ) || ! array_key_exists( $section, $section_tabs ) ) {
					$section  = 'main';
					$settings = $sections;
				}

				add_settings_section(
					$this->func . '_settings_' . $tab . '_' . $section,
					__return_null(),
					'__return_false',
					$this->func . '_settings_' . $tab . '_' . $section
				);

				foreach ( $settings as $option ) {
					// For backwards compatibility.
					if ( empty( $option['id'] ) ) {
						continue;
					}

					$args = wp_parse_args(
						$option,
						array(
							'section'        => $section,
							'id'             => null,
							'desc'           => '',
							'name'           => '',
							'size'           => null,
							'options'        => '',
							'std'            => '',
							'min'            => null,
							'max'            => null,
							'step'           => null,
							'select2'        => null,
							'multiple'       => null,
							'placeholder'    => null,
							'allow_blank'    => true,
							'readonly'       => false,
							'disabled'       => false,
							'buttons'        => null,
							'wpautop'        => null,
							'teeny'          => null,
							'editor_tinymce' => true,
							'editor_html'    => true,
							'default_editor' => '',
							'mode'           => '',
							'line_numbers'   => true,
							'indent_size'    => 2,
							'indent_tabs'    => false,
							'tab'            => null,
							'tooltip_title'  => false,
							'tooltip_desc'   => false,
							'field_class'    => '',
							'parent'         => array(
								'location' => $this->location,
								'slug'     => $this->slug,
								'func'     => $this->func,
								'version'  => $this->version,
							),
						)
					);

					$core_fields = array(
						'header',
						'checkbox',
						'color',
						'descriptive_text',
						'editor',
						'html',
						'multicheck',
						'number',
						'password',
						'radio',
						'select',
						'text',
						'textarea',
						'upload',
						'hook',
					);

					// Setup callbacks.
					$callback = array( $this, 'missing_callback' );

					if ( in_array( $option['type'], $core_fields ) ) {
						if ( method_exists( $this, $option['type'] . '_callback' ) ) {
							$callback = array( $this, $option['type'] . '_callback' );
						}
					} elseif ( function_exists( $this->func . '_' . $option['type'] . '_callback' ) ) {
						$callback = $this->func . '_' . $option['type'] . '_callback';
					} elseif ( function_exists( 'origami_' . $option['type'] . '_callback' ) ) {
						$callback = 'origami_' . $option['type'] . '_callback';
					}

					add_settings_field(
						$this->func . '_settings[' . $option['id'] . ']',
						$args['name'],
						$callback,
						$this->func . '_settings_' . $tab . '_' . $section,
						$this->func . '_settings_' . $tab . '_' . $section,
						apply_filters(
							$this->func . '_settings_allowed_args',
							$args,
							$option
						)
					);
				}
			}
		}

		register_setting( $this->func . '_settings', $this->func . '_settings', array( 'sanitize_callback' => array( $this, 'settings_sanitize' ) ) );
	}


	/**
	 * Settings sanitization
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $input The value entered in the field.
	 * @global      array ${$this->func . '_options'} The options array
	 * @return      array $output The sanitized values
	 */
	public function settings_sanitize( $input = array() ) {
		global ${$this->func . '_options'};

		if ( isset( $_REQUEST[ $this->slug . '-nonce' ] ) ) {
			check_admin_referer( $this->slug . '-nonce', $this->slug . '-nonce' );
		}

		$doing_section = false;
		$post          = wp_unslash( $_POST );

		if ( ! empty( $post['_wp_http_referer'] ) ) {
			$doing_section = true;
		}

		$setting_types = $this->get_registered_settings_types();
		$input         = $input ? $input : array();
		$output        = '';

		if ( $doing_section ) {
			parse_str( $post['_wp_http_referer'], $referrer );

			$tab     = isset( $referrer['tab'] ) ? $referrer['tab'] : $this->default_tab;
			$section = isset( $referrer['section'] ) ? $referrer['section'] : 'main';

			if ( ! empty( $post[ $this->func . '_section_override' ] ) ) {
				$section = sanitize_text_field( $post[ $this->func . '_section_override' ] );
			}

			$setting_types = $this->get_registered_settings_types( $tab, $section );

			$input  = apply_filters( $this->func . '_settings_' . $tab . '_sanitize', $input );
			$input  = apply_filters( $this->func . '_settings_' . $tab . '_' . $section . '_sanitize', $input );
			$output = array_merge( ${$this->func . '_options'}, $input );

			foreach ( $setting_types as $key => $type ) {
				if ( empty( $type ) ) {
					continue;
				}

				// Bypass non-setting settings.
				$non_setting_types = apply_filters(
					$this->func . '_non_setting_types',
					array(
						'header',
						'descriptive_text',
						'hook',
					)
				);

				if ( in_array( $type, $non_setting_types, true ) ) {
					continue;
				}

				if ( array_key_exists( $key, $output ) ) {
					$output[ $key ] = apply_filters( $this->func . '_settings_sanitize_' . $type, $output[ $key ], $key );
					$output[ $key ] = apply_filters( $this->func . '_settings_sanitize', $output[ $key ], $key );
				}

				if ( empty( $input[ $key ] ) ) {
					unset( $output[ $key ] );

					return $output;
				}

				if ( $doing_section ) {
					switch ( $type ) {
						case 'checkbox':
							if ( array_key_exists( $key, $input ) && '-1' === $output[ $key ] ) {
								$output[ $key ] = 'unchecked';
							}
							break;
						case 'multicheck':
							$settings = $this->get_registered_settings_types( $tab, $section );

							if ( array_key_exists( $key, $settings ) && is_array( $settings[ $key ] ) ) {
								foreach ( $settings[ $key ] as $option ) {
									$output[ $key ][ $option ] = 'unchecked';

									if ( array_key_exists( $key, $input ) ) {
										if ( array_key_exists( $option, $input[ $key ] ) ) {
											$output[ $key ][ $option ] = $input[ $key ][ $option ];
										}
									} elseif ( array_key_exists( 'std', $settings[ $key ] ) ) {
										if ( ! is_array( $input[ $key ] ) || empty( $input[ $key ] ) ) {
											$output[ $key ][ $option ] = $settings[ $key ]['std'][ $option ];
										}
									}
								}
							}
							break;
						case 'text':
							if ( array_key_exists( $key, $input ) && empty( $input[ $key ] ) ) {
								unset( $output[ $key ] );
							}
							break;
						default:
							if ( ( array_key_exists( $key, $input ) && empty( $input[ $key ] ) ) || ( array_key_exists( $key, $output ) && ! array_key_exists( $key, $input ) ) ) {
								unset( $output[ $key ] );
							}
							break;
					}
				}
			}
		}

		return $output;
	}


	/**
	 * Flattens the set of registered settings and their type so we can easily sanitize all settings
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       mixed $filter_tab bool|string A tab to filter by.
	 * @param       mixed $filter_section bool|string A section to filter by.
	 * @return      array Key is the setting ID, value is the type of setting it is registered as
	 */
	public function get_registered_settings_types( $filter_tab = false, $filter_section = false ) {
		$settings      = $this->get_registered_settings();
		$setting_types = array();

		foreach ( $settings as $tab_id => $tab ) {
			if ( false !== $filter_tab && $filter_tab !== $tab_id ) {
				continue;
			}

			foreach ( $tab as $section_id => $section_or_setting ) {
				// See if we have a setting registered at the tab level for backwards compatibility.
				if ( false !== $filter_section && is_array( $section_or_setting ) && array_key_exists( 'type', $section_or_setting ) ) {
					$setting_types[ $section_or_setting['id'] ] = $section_or_setting['type'];
					continue;
				}

				if ( false !== $filter_section && $filter_section !== $section_id ) {
					continue;
				}

				foreach ( $section_or_setting as $section_settings ) {
					if ( ! is_array( $section_settings ) ) {
						continue;
					}

					if ( array_key_exists( 'id', $section_settings ) && array_key_exists( 'type', $section_settings ) ) {
						$setting_types[ $section_settings['id'] ] = $section_settings['type'];
					}
				}
			}
		}

		return $setting_types;
	}


	/**
	 * Sanitize text fields
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $input The value entered in the field.
	 * @return      string $input The sanitized value
	 */
	public function sanitize_text_field( $input ) {
		$tags = array(
			'p'      => array(
				'class' => array(),
				'id'    => array(),
			),
			'span'   => array(
				'class' => array(),
				'id'    => array(),
			),
			'a'      => array(
				'href'   => array(),
				'target' => array(),
				'title'  => array(),
				'class'  => array(),
				'id'     => array(),
			),
			'strong' => array(),
			'em'     => array(),
			'br'     => array(),
			'img'    => array(
				'src'   => array(),
				'title' => array(),
				'alt'   => array(),
				'class' => array(),
				'id'    => array(),
			),
			'div'    => array(
				'class' => array(),
				'id'    => array(),
			),
			'ul'     => array(
				'class' => array(),
				'id'    => array(),
			),
			'ol'     => array(
				'class' => array(),
				'id'    => array(),
			),
			'li'     => array(
				'class' => array(),
				'id'    => array(),
			),
		);

		$allowed_tags = apply_filters( $this->func . '_allowed_html_tags', $tags );

		return trim( wp_kses( $input, $allowed_tags ) );
	}


	/**
	 * Sanitize HTML Class Names
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string|array $class HTML Class Name(s).
	 * @return      string $class
	 */
	public function sanitize_html_class( $class = '' ) {
		if ( is_string( $class ) ) {
			$class = sanitize_html_class( $class );
		} elseif ( is_array( $class ) ) {
			$class = array_values( array_map( 'sanitize_html_class', $class ) );
			$class = implode( ' ', array_unique( $class ) );
		}

		return $class;
	}


	/**
	 * Sanitizes a string key
	 *
	 * Keys are used as internal identifiers. Alphanumeric characters, dashes,
	 * underscores, stops, colons and slashes are allowed
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $key String key.
	 * @return      string Sanitized key
	 */
	public function sanitize_key( $key ) {
		$raw_key = $key;
		$key     = preg_replace( '/[^a-zA-Z0-9_\-\.\:\/]/', '', $key );

		return apply_filters( $this->func . '_sanitize_key', $key, $raw_key );
	}


	/**
	 * Reset a given section
	 *
	 * @since       1.0.0
	 * @return      void
	 */
	public function reset_section() {
		global ${$this->func . '_options'};

		if ( isset( $_REQUEST[ $this->slug . '-reset-nonce' ] ) ) {
			check_admin_referer( $this->slug . '-reset-nonce', $this->slug . '-reset-nonce' );
		}

		$get                 = wp_unslash( $_GET );
		$active_tab          = isset( $get['tab'] ) && array_key_exists( $get['tab'], $this->get_settings_tabs() ) ? $get['tab'] : $this->default_tab;
		$registered_sections = $this->get_settings_tab_sections( $active_tab );
		$sections            = $registered_sections;
		$key                 = 'main';
		$settings            = $this->get_registered_settings();

		if ( is_array( $sections ) ) {
			$key = key( $sections );
		}

		$section = isset( $get['section'] ) && ! empty( $registered_sections ) && array_key_exists( $get['section'], $registered_sections ) ? $get['section'] : $key;

		if ( ! empty( $settings ) ) {
			if ( array_key_exists( $active_tab, $settings ) ) {
				if ( ! empty( $settings[ $active_tab ] ) && array_key_exists( $section, $settings[ $active_tab ] ) ) {
					foreach ( $settings[ $active_tab ][ $section ] as $setting ) {
						if ( array_key_exists( $setting['id'], ${$this->func . '_options'} ) ) {
							unset( ${$this->func . '_options'}[ $setting['id'] ] );
						}
					}
				}
			}
		}

		$this->update_options( ${$this->func . '_options'} );

		$tab_url = remove_query_arg(
			array(
				$this->slug . '-action',
				$this->slug . '-nonce',
				$this->slug . '-reset-nonce',
			)
		);

		wp_safe_redirect(
			add_query_arg(
				array(
					'updated' => 'true',
				),
				$tab_url,
			)
		);

		exit;
	}


	/**
	 * Parse args for a given field
	 *
	 * The below is for my reference and not representative of current features.
	 *
	 * $max         = number, range, date, datetime-local, month, time and week.
	 * $maxlength   = text, search, url, tel, email, password.
	 * $min         = number, range, date, datetime-local, month, time and week.
	 * $minlength   = text, search, url, tel, email, password.
	 * $multiple    = email, file, select.
	 * $pattern     = text, date, search, url, tel, email, and password.
	 * $placeholder = select, text, search, url, tel, email, and password.
	 * $size        = text, search, tel, url, email, and password.
	 * $step        = number, range, date, datetime-local, month, time and week.
	 *
	 * @access      private
	 * @since       1.0.0
	 * @param       array $args Arguments to parse.
	 * @return      array $parsed The parsed arguments
	 */
	private function parse_args( $args ) {
		global ${$this->func . '_options'};

		$type   = $args['type'];
		$parsed = array();

		$parsed['checked']     = 'unchecked';
		$parsed['class']       = ( ! empty( $args['class'] ) ) ? $this->sanitize_html_class( $args['class'] ) : '';
		$parsed['desc']        = $args['desc'];
		$parsed['disabled']    = ( true === $args['disabled'] ) ? ' disabled' : '';
		$parsed['name']        = $this->func . '_settings[' . $this->sanitize_key( $args['id'] ) . ']';
		$parsed['id']          = $this->func . '_settings[' . $this->sanitize_key( $args['id'] ) . ']';
		$parsed['placeholder'] = ( ! empty( $args['placeholder'] ) ) ? $args['placeholder'] : '';
		$parsed['readonly']    = ( true === $args['readonly'] ) ? ' readonly' : '';
		$parsed['std']         = $args['std'];

		switch ( $type ) {
			case 'checkbox':
				if ( isset( ${$this->func . '_options'}[ $args['id'] ] ) ) {
					$parsed['checked'] = checked( 'checked', ${$this->func . '_options'}[ $this->sanitize_key( $args['id'] ) ], false );
				} elseif ( isset( $args['std'] ) ) {
					$parsed['checked'] = checked( 'checked', $args['std'], false );
				}

				break;
			case 'editor':
				if ( isset( ${$this->func . '_options'}[ $args['id'] ] ) ) {
					$parsed['value'] = ${$this->func . '_options'}[ $args['id'] ];

					if ( ( ! isset( $args['allow_blank'] ) || false === $args['allow_blank'] ) && empty( $parsed['value'] ) ) {
						$parsed['value'] = isset( $args['std'] ) ? $args['std'] : '';
					}
				} elseif ( isset( $args['std'] ) ) {
					$parsed['value'] = isset( $args['std'] ) ? $args['std'] : '';
				}

				$parsed['id'] = $this->func . '_settings_' . $this->sanitize_key( $args['id'] );

				$parsed['buttons']        = isset( $args['buttons'] ) ? $args['buttons'] : true;
				$parsed['default_editor'] = isset( $args['default_editor'] ) ? $args['default_editor'] : '';
				$parsed['editor_html']    = isset( $args['editor_html'] ) ? $args['editor_html'] : true;
				$parsed['editor_tinymce'] = isset( $args['editor_tinymce'] ) ? $args['editor_tinymce'] : true;
				$parsed['rows']           = isset( $args['size'] ) ? $args['size'] : '20';
				$parsed['teeny']          = isset( $args['teeny'] ) ? $args['teeny'] : false;
				$parsed['wpautop']        = isset( $args['wpautop'] ) ? $args['wpautop'] : true;

				break;
			case 'html':
				if ( isset( ${$this->func . '_options'}[ $args['id'] ] ) ) {
					$parsed['value'] = ${$this->func . '_options'}[ $args['id'] ];

					if ( ( ! isset( $args['allow_blank'] ) || false === $args['allow_blank'] ) && empty( $parsed['value'] ) ) {
						$parsed['value'] = isset( $args['std'] ) ? $args['std'] : '';
					}
				} elseif ( isset( $args['std'] ) ) {
					$parsed['value'] = isset( $args['std'] ) ? $args['std'] : '';
				}

				$mode         = ( isset( $args['mode'] ) ) ? $args['mode'] : 'text/x-php';
				$line_numbers = ( isset( $args['line_numbers'] ) ) ? $args['line_numbers'] : true;
				$indent_size  = ( isset( $args['indent_size'] ) ) ? absint( $args['indent_size'] ) : 2;
				$indent_tabs  = ( isset( $args['indent_tabs'] ) ) ? $args['indent_tabs'] : false;

				$parsed['cm_data'] = array(
					'editor_mode'  => $mode,
					'line_numbers' => $line_numbers,
					'indent_size'  => $indent_size,
					'indent_tabs'  => $indent_tabs,
				);

				break;
			case 'multicheck':
				if ( isset( $args['options'] ) && ! empty( $args['options'] ) ) {
					foreach ( $args['options'] as $key => $value ) {
						$key = $this->sanitize_key( $key );

						$parsed['options'][ $key ] = array(
							'checked' => 'unchecked',
							'value'   => $value,
						);

						if ( isset( ${$this->func . '_options'}[ $args['id'] ][ $key ] ) ) {
							$parsed['options'][ $key ]['checked'] = checked( 'checked', ${$this->func . '_options'}[ $args['id'] ][ $key ], false );
						} elseif ( isset( $args['std'][ $key ] ) ) {
							$parsed['options'][ $key ]['checked'] = checked( true, true, false );
						}
					}
				}

				break;
			case 'number':
				if ( isset( ${$this->func . '_options'}[ $args['id'] ] ) ) {
					$parsed['value'] = ${$this->func . '_options'}[ $args['id'] ];
				} elseif ( isset( $args['std'] ) ) {
					$parsed['value'] = isset( $args['std'] ) ? $args['std'] : '';
				}

				$parsed['max']  = ( isset( $args['max'] ) && is_numeric( $args['max'] ) ) ? $args['max'] : 999999;
				$parsed['min']  = ( isset( $args['min'] ) && is_numeric( $args['min'] ) ) ? $args['min'] : 0;
				$parsed['size'] = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';
				$parsed['step'] = ( isset( $args['step'] ) && is_numeric( $args['step'] ) ) ? $args['step'] : 1;

				// Just in case people get cute, make sure min is lower than max.
				if ( $parsed['min'] > $parsed['max'] ) {
					$min           = $parsed['max'];
					$parsed['max'] = $parsed['min'];
					$parsed['min'] = $min;
				}

				break;
			case 'radio':
				if ( isset( $args['options'] ) && ! empty( $args['options'] ) ) {
					foreach ( $args['options'] as $key => $value ) {
						$key = $this->sanitize_key( $key );

						$parsed['options'][ $key ] = array(
							'checked' => '',
							'value'   => $value,
						);

						if ( isset( ${$this->func . '_options'}[ $args['id'] ] ) ) {
							$parsed['options'][ $key ]['checked'] = checked( $key, ${$this->func . '_options'}[ $args['id'] ], false );
						} elseif ( isset( $args['std'] ) && $args['std'] === $key && ! isset( ${$this->func . '_options'}[ $args['id'] ] ) ) {
							$parsed['options'][ $key ]['checked'] = checked( true, true, false );
						}
					}
				}

				break;
			case 'select':
				if ( isset( $args['options'] ) && ! empty( $args['options'] ) ) {
					if ( isset( ${$this->func . '_options'}[ $args['id'] ] ) ) {
						$value = ${$this->func . '_options'}[ $args['id'] ];
					} elseif ( isset( $args['std'] ) ) {
						$value = isset( $args['std'] ) ? $args['std'] : '';
					}

					$parsed['multiple'] = isset( $args['multiple'] ) ? $args['multiple'] : false;
					$parsed['select2']  = isset( $args['select2'] ) && $args['select2'] ? 'origami-select2' : '';
					$parsed['size']     = isset( $args['size'] ) ? ' style="width: ' . $args['size'] . '"' : '';

					if ( $parsed['multiple'] ) {
						$parsed['name'] .= '[]';
					}

					if ( is_array( $args['options'][0] ) ) {
						foreach ( $args['options'] as $group_key => $option_group ) {
							$parsed['options'][ $group_key ]['label'] = $args['options'][ $group_key ]['label'];

							foreach ( $args['options'][ $group_key ]['options'] as $key => $label ) {
								$parsed['options'][ $group_key ]['options'][ $key ] = array(
									'selected' => '',
									'value'    => $label,
								);

								if ( isset( $args['multiple'] ) && true === $args['multiple'] ) {
									if ( is_array( $value ) && in_array( $key, $value ) ) {
										$parsed['options'][ $group_key ]['options'][ $key ]['selected'] = 'selected="true"';
									}
								} elseif ( ! isset( $args['multiple'] ) || true !== $args['multiple'] ) {
									$parsed['options'][ $group_key ]['options'][ $key ]['selected'] = selected( $key, $value, false );
								}
							}
						}
					} else {
						foreach ( $args['options'] as $key => $label ) {
							$parsed['options'][ $key ] = array(
								'selected' => '',
								'value'    => $label,
							);

							if ( isset( $args['multiple'] ) && true === $args['multiple'] ) {
								if ( is_array( $value ) && in_array( $key, $value ) ) {
									$parsed['options'][ $key ]['selected'] = 'selected="true"';
								}
							} elseif ( ! isset( $args['multiple'] ) || true !== $args['multiple'] ) {
								$parsed['options'][ $key ]['selected'] = selected( $key, $value, false );
							}
						}
					}
				}

				break;
			default:
				if ( isset( ${$this->func . '_options'}[ $args['id'] ] ) ) {
					$parsed['value'] = ${$this->func . '_options'}[ $args['id'] ];
				} elseif ( isset( $args['std'] ) ) {
					$parsed['value'] = isset( $args['std'] ) ? $args['std'] : '';
				}

				$parsed['size'] = ( isset( $args['size'] ) && ! is_null( $args['size'] ) ) ? $args['size'] : 'regular';

				break;
		}

		return $parsed;
	}


	/**
	 * Header callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function header_callback( $args ) {
		echo '<div class="origami-header-wrap">';
		do_action( $this->func . '_after_setting_output', $args );
		echo '</div>';
	}


	/**
	 * Checkbox callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function checkbox_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-checkbox-wrap">';
		echo '<input name="' . esc_attr( $parsed['name'] ) . '" type="hidden" value="unchecked" />';
		echo '<input class="' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '" name="' . esc_attr( $parsed['name'] ) . '" type="checkbox" value="checked" ' . wp_kses_post( $parsed['checked'] ) . esc_attr( $parsed['disabled'] ) . esc_attr( $parsed['readonly'] ) . '/>&nbsp;';
		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Color callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the settings.
	 * @return      void
	 */
	public function color_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-color-wrap">';
		echo '<input class="origami-color-picker ' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '" name="' . esc_attr( $parsed['name'] ) . '" type="text" value="' . esc_attr( $parsed['value'] ) . '" data-default-color="' . esc_attr( $parsed['std'] ) . '" ' . esc_attr( $parsed['disabled'] ) . esc_attr( $parsed['readonly'] ) . '/>&nbsp;';
		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Descriptive text callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function descriptive_text_callback( $args ) {
		echo '<div class="origami-descriptive-text-wrap">';
		echo wp_kses_post( $args['desc'] );

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Editor callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function editor_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-editor-wrap">';

		wp_editor(
			stripslashes( $parsed['value'] ),
			esc_attr( $parsed['id'] ),
			array(
				'default_editor' => $parsed['default_editor'],
				'editor_class'   => esc_attr( $parsed['class'] ),
				'media_buttons'  => (bool) $parsed['buttons'],
				'quicktags'      => (bool) $parsed['editor_html'],
				'teeny'          => (bool) $parsed['teeny'],
				'textarea_name'  => esc_attr( $parsed['name'] ),
				'textarea_rows'  => absint( $parsed['rows'] ),
				'tinymce'        => (bool) $parsed['editor_tinymce'],
				'wpautop'        => (bool) $parsed['wpautop'],
			)
		);

		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * HTML callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function html_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-html-wrap">';
		echo '<textarea class="large-text origami-html ' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '" name="' . esc_attr( $parsed['name'] ) . '" data-editor-mode="' . esc_attr( $parsed['cm_data']['editor_mode'] ) . '" data-line-numbers="' . esc_attr( $parsed['cm_data']['line_numbers'] ) . '" data-indent-size="' . esc_attr( $parsed['cm_data']['indent_size'] ) . '" data-indent-tabs="' . esc_attr( $parsed['cm_data']['indent_tabs'] ) . '" style="display: none">' . esc_textarea( $parsed['value'] ) . '</textarea>&nbsp;';
		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Multicheck callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function multicheck_callback( $args ) {
		$parsed = $this->parse_args( $args );

		if ( ! empty( $parsed['options'] ) ) {
			echo '<div class="origami-multicheck-wrap">';

			foreach ( $parsed['options'] as $key => $data ) {
				echo '<input name="' . esc_attr( $parsed['name'] ) . '[' . esc_attr( $key ) . ']" type="hidden" value="unchecked" />';
				echo '<input class="' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '[' . esc_attr( $key ) . ']" name="' . esc_attr( $parsed['id'] ) . '[' . esc_attr( $key ) . ']" type="checkbox" value="checked" ' . ( isset( $data['checked'] ) ? wp_kses_post( $data['checked'] ) : '' ) . esc_attr( $parsed['disabled'] ) . esc_attr( $parsed['readonly'] ) . '/>&nbsp;';
				echo '<label for="' . esc_attr( $parsed['name'] ) . '[' . esc_attr( $key ) . ']">' . wp_kses_post( $data['value'] ) . '</label><br />';
			}

			echo '<span class="description">' . wp_kses_post( $parsed['desc'] ) . '</span>';

			do_action( $this->func . '_after_setting_output', $args );

			echo '</div>';
		}
	}


	/**
	 * Number callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function number_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-number-wrap">';
		echo '<input class="' . esc_attr( $parsed['class'] ) . ' ' . esc_attr( $parsed['size'] ) . '-text" id="' . esc_attr( $parsed['id'] ) . '" max="' . esc_attr( $parsed['max'] ) . '" min="' . esc_attr( $parsed['min'] ) . '" name="' . esc_attr( $parsed['name'] ) . '" placeholder="' . esc_attr( $parsed['placeholder'] ) . '" step="' . esc_attr( $parsed['step'] ) . '" type="number" value="' . esc_attr( $parsed['value'] ) . '" ' . esc_attr( $parsed['disabled'] ) . esc_attr( $parsed['readonly'] ) . '/>&nbsp;';
		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Password callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the settings.
	 * @return      void
	 */
	public function password_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-password-wrap">';
		echo '<input class="' . esc_attr( $parsed['size'] ) . '-text ' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '" name="' . esc_attr( $parsed['name'] ) . '" placeholder="' . esc_attr( $parsed['placeholder'] ) . '" type="password" value="' . esc_attr( $parsed['value'] ) . '" ' . esc_attr( $parsed['disabled'] ) . esc_attr( $parsed['readonly'] ) . '/>&nbsp;';
		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Radio callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function radio_callback( $args ) {
		$parsed = $this->parse_args( $args );

		if ( ! empty( $parsed['options'] ) ) {
			echo '<div class="origami-radio-wrap">';

			foreach ( $parsed['options'] as $key => $data ) {
				echo '<input class="' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '[' . esc_attr( $key ) . ']" name="' . esc_attr( $parsed['id'] ) . '" type="radio" value="' . esc_attr( $key ) . '" ' . ( isset( $data['checked'] ) ? wp_kses_post( $data['checked'] ) : '' ) . esc_attr( $parsed['disabled'] ) . esc_attr( $parsed['readonly'] ) . '/>&nbsp;';
				echo '<label for="' . esc_attr( $parsed['name'] ) . '[' . esc_attr( $key ) . ']">' . esc_html( $data['value'] ) . '</label><br />';
			}

			echo '<span class="description">' . wp_kses_post( $parsed['desc'] ) . '</span>';

			do_action( $this->func . '_after_setting_output', $args );

			echo '</div>';
		}
	}


	/**
	 * Select callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function select_callback( $args ) {
		$parsed = $this->parse_args( $args );

		$nonce = isset( $args['data']['nonce'] ) ? 'data-nonce="' . sanitize_text_field( $args['data']['nonce'] ) . '" ' : '';

		echo '<div class="origami-select-' . ( ( $parsed['multiple'] ) ? 'multiple-' : '' ) . 'wrap">';
		echo '<select ' . esc_attr( $nonce ) . ' class="' . esc_attr( $parsed['class'] ) . ' ' . esc_attr( $parsed['select2'] ) . '" id="' . esc_attr( $parsed['id'] ) . '" name="' . esc_attr( $parsed['name'] ) . '" data-dropdown-css-class="origami-select2-dropdown" data-selection-css-class="origami-select2-selection" data-placeholder="' . esc_attr( $parsed['placeholder'] ) . '"' . esc_attr( $parsed['size'] ) . ( ( $parsed['multiple'] ) ? ' multiple="true"' : '' ) . ' />';

		if ( is_array( $parsed['options'][0] ) ) {
			foreach ( $parsed['options'] as $option_group ) {
				echo '<optgroup label="' . esc_attr( $option_group['label'] ) . '">';

				foreach ( $option_group['options'] as $key => $data ) {
					echo '<option value="' . esc_attr( $key ) . '" ' . wp_kses_post( $data['selected'] ) . '>' . esc_html( $data['value'] ) . '</option>';
				}

				echo '</optgroup>';
			}
		} else {
			foreach ( $parsed['options'] as $key => $data ) {
				echo '<option value="' . esc_attr( $key ) . '" ' . wp_kses_post( $data['selected'] ) . '>' . esc_html( $data['value'] ) . '</option>';
			}
		}

		echo '</select>&nbsp;';
		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Text callback
	 *
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function text_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-text-wrap">';
		echo '<input class="' . esc_attr( $parsed['size'] ) . '-text ' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '" name="' . esc_attr( $parsed['name'] ) . '" placeholder="' . esc_attr( $parsed['placeholder'] ) . '" type="text" value="' . esc_attr( $parsed['value'] ) . '" ' . esc_attr( $parsed['disabled'] ) . esc_attr( $parsed['readonly'] ) . '/>&nbsp;';
		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Textarea callback
	 *
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function textarea_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-textarea-wrap">';
		echo '<textarea class="' . esc_attr( $parsed['size'] ) . '-text ' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '" name="' . esc_attr( $parsed['name'] ) . '">' . esc_textarea( $parsed['value'] ) . '</textarea><br />';
		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Upload callback
	 *
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function upload_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-upload-wrap">';
		echo '<input class="' . esc_attr( $parsed['size'] ) . '-text ' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '" name="' . esc_attr( $parsed['name'] ) . '" placeholder="' . esc_attr( $parsed['placeholder'] ) . '" type="text" value="' . esc_attr( $parsed['value'] ) . '" ' . esc_attr( $parsed['disabled'] ) . esc_attr( $parsed['readonly'] ) . '/>&nbsp;';
		echo '<span>&nbsp;<input type="button" class="origami_upload_button button-secondary" value="' . esc_attr__( 'Upload File', 'origami' ) . '" /></span><br />';
		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * License field callback
	 * TODO: Update and make sure this still works once I have access to SL
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 * @deprecated
	 */
	public function license_key_callback( $args ) {
		$parsed = $this->parse_args( $args );

		echo '<div class="origami-license-wrap">';
		echo '<input class="' . esc_attr( $parsed['size'] ) . '-text ' . esc_attr( $parsed['class'] ) . '" id="' . esc_attr( $parsed['id'] ) . '" name="' . esc_attr( $parsed['name'] ) . '" placeholder="' . esc_attr( $parsed['placeholder'] ) . '" type="text" value="' . esc_attr( $parsed['value'] ) . '" ' . esc_attr( $parsed['disabled'] ) . esc_attr( $parsed['readonly'] ) . '/>&nbsp;';

		$valid_license = false;

		if ( 'network' === $this->location && get_network_option( null, $args['options']['is_valid_license_option'] ) ) {
			$valid_license = true;
		} elseif ( get_option( $args['options']['is_valid_license_option'] ) ) {
			$valid_license = true;
		}

		if ( $valid_license ) {
			echo '<input class="button-secondary" name="' . esc_attr( $parsed['name'] ) . '_deactivate" type="submit" value="' . esc_attr__( 'Deactivate License', 'origami' ) . '"/>';
		}

		echo '<label for="' . esc_attr( $parsed['name'] ) . '">' . wp_kses_post( $parsed['desc'] ) . '</label>';

		wp_nonce_field( esc_attr( $parsed['id'] ) . '_nonce', esc_attr( $args['id'] ) . '_nonce' );

		do_action( $this->func . '_after_setting_output', $args );

		echo '</div>';
	}


	/**
	 * Hook callback
	 *
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function hook_callback( $args ) {
		do_action( $this->func . '_' . $args['id'], $args );
	}


	/**
	 * Missing callback
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed by the setting.
	 * @return      void
	 */
	public function missing_callback( $args ) {
		$missing_callback  = sprintf(
			// Translators: The passed ID that has no corresponding callback.
			__( 'The callback function used for the %s setting is missing.', 'origami' ),
			'<code>' . esc_html( $args['id'] ) . '</code>'
		);
		$missing_callback .= '<br />';
		$missing_callback .= '<strong>' . __( 'Expected:', 'origami' ) . '</strong> <code>' . esc_html( $args['func'] ) . esc_html( $args['type'] ) . '</code>';

		echo wp_kses_post( $missing_callback );
	}


	/**
	 * Check if we should load admin scripts
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $hook The hook for the current page.
	 * @return      bool true if we should load scripts, false otherwise
	 */
	public function load_scripts( $hook ) {
		global $origami_pages;

		$ret   = false;
		$pages = apply_filters( 'origami_admin_pages', $origami_pages );

		if ( is_array( $pages ) && in_array( $hook, $pages, true ) ) {
			$ret = true;
		}

		return (bool) apply_filters( $this->func . 'load_scripts', $ret );
	}


	/**
	 * Processes all actions sent via POST and GET by looking for the '$func-action'
	 * request and running do_action() to call the function
	 *
	 * @since       1.0.0
	 * @return      void
	 */
	public function process_actions() {
		if ( isset( $_REQUEST[ $this->slug . '-nonce' ] ) ) {
			check_admin_referer( $this->slug . '-nonce', $this->slug . '-nonce' );
		}

		$post = wp_unslash( $_POST );

		if ( isset( $post['submit'] ) ) {
			if ( isset( $post[ $this->func . '_settings' ] ) && ! empty( $post[ $this->func . '_settings' ] ) ) {
				$did_update = $this->update_options( $post[ $this->func . '_settings' ] );

				if ( $did_update ) {
					wp_safe_redirect( add_query_arg( 'updated', 'true' ) );
					exit;
				}
			}

			return;
		}

		if ( isset( $post[ $this->slug . '-action' ] ) ) {
			do_action( $this->func . '_settings_' . $post[ $this->slug . '-action' ], $post );
		}

		$get = wp_unslash( $_GET );

		if ( isset( $get[ $this->slug . '-action' ] ) ) {
			do_action( $this->func . '_settings_' . $get[ $this->slug . '-action' ], $get );
		}
	}


	/**
	 * Enqueue scripts
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       string $hook The hook for the requesting page.
	 * @return      void
	 */
	public function enqueue_scripts( $hook ) {
		if ( ! apply_filters( $this->func . '_load_admin_scripts', $this->load_scripts( $hook ), $hook ) ) {
			return;
		}

		// Use minified libraries if SCRIPT_DEBUG is turned off.
		$suffix   = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
		$ui_style = ( get_user_option( 'admin_color' ) === 'classic' ) ? 'classic' : 'fresh';
		$url_path = str_replace( WP_CONTENT_DIR, WP_CONTENT_URL, dirname( __FILE__ ) );

		// If working on Windows, we have to be hacky.
		if ( strtoupper( substr( PHP_OS, 0, 3 ) ) === 'WIN' ) {
			$url_path = str_replace( '\\', '/', str_replace( str_replace( '/', '\\', WP_CONTENT_DIR ), WP_CONTENT_URL, $url_path ) );
			$url_data = wp_parse_url( $url_path );
			$url_path = $url_data['path'];
		}

		// Setup versioning for internal assets.
		$css_ver = gmdate( 'ymd-Gis', filemtime( trailingslashit( dirname( __FILE__ ) ) . 'assets/css/admin' . $suffix . '.css' ) );
		$js_ver  = gmdate( 'ymd-Gis', filemtime( trailingslashit( dirname( __FILE__ ) ) . 'assets/js/admin' . $suffix . '.js' ) );

		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_script( 'jquery-ui-tooltip' );
		wp_enqueue_media();
		wp_enqueue_style( 'origami-jquery-ui-css', $url_path . '/assets/css/jquery-ui-' . $ui_style . '.min.css', array(), '1.0.0' );
		wp_enqueue_script( 'media-upload' );
		wp_enqueue_style( 'thickbox' );
		wp_enqueue_script( 'thickbox' );
		wp_enqueue_style( 'origami-select2', $url_path . '/assets/js/select2/css/select2' . $suffix . '.css', array(), '4.0.13' );
		wp_enqueue_script( 'origami-select2', $url_path . '/assets/js/select2/js/select2.full' . $suffix . '.js', array( 'jquery' ), '4.1.0-rc.0', true );
		wp_enqueue_script( 'wp-codemirror' );
		wp_enqueue_style( 'wp-codemirror' );

		wp_enqueue_style( 'origami', $url_path . '/assets/css/admin' . $suffix . '.css', array(), $this->version . '-' . $css_ver );
		wp_enqueue_script( 'origami', $url_path . '/assets/js/admin' . $suffix . '.js', array( 'jquery' ), $this->version . '-' . $js_ver, true );
		wp_localize_script(
			'origami',
			'origami_vars',
			apply_filters(
				$this->func . 'localize_script',
				array(
					'func'               => $this->func,
					'image_media_button' => __( 'Insert Image', 'origami' ),
					'image_media_title'  => __( 'Select Image', 'origami' ),
					'is_rtl'             => is_rtl() ? 'rtl' : 'ltr',
				)
			)
		);
	}


	/**
	 * Add tooltips
	 *
	 * @access      public
	 * @since       1.0.0
	 * @param       array $args Arguments passed to the field.
	 * @return      void
	 */
	public function add_setting_tooltip( $args ) {
		if ( ! empty( $args['tooltip_title'] ) && ! empty( $args['tooltip_desc'] ) ) {
			?>
			<span alt="f223" class="origami-help-tip dashicons dashicons-editor-help" title="<strong><?php echo esc_html( $args['tooltip_title'] ); ?></strong>: <?php echo esc_html( $args['tooltip_desc'] ); ?>"></span>
			<?php
		}
	}


	/**
	 * Get the current library version
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      string The current version number
	 */
	public function get_version() {
		return $this->version;
	}
}
