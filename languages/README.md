# Origami

This folder contains translation files for Origami.

Do not store custom translations in this folder, they will be deleted on updates.
Store custom translations in `wp-content/languages/origami`.
