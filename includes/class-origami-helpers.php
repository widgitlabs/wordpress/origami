<?php
/**
 * Helper functions for Origami
 *
 * It turns out that sometimes, common-sense things need a helping hand - go
 * figure. This file contains helper functions added to handle common-use cases
 * that PHP and WordPress don't handle well on their own, made accessible to
 * third-party plugins because... they're common-use.
 *
 * @package     WidgitLabs\Origami\Sysinfo
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Origami_Helpers' ) ) {

	/**
	 * Main Origami_Helpers class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	class Origami_Helpers {


		/**
		 * The page slug
		 *
		 * @var         string $slug The page slug
		 * @access      protected
		 * @since       1.0.0
		 */
		protected $slug;


		/**
		 * The page slug for names
		 *
		 * @var         string $func The page slug for names
		 * @access      protected
		 * @since       1.0.0
		 */
		protected $func;


		/**
		 * Class constructor
		 *
		 * @access      public
		 * @since       1.0.0
		 * @param       string $slug The item slug.
		 * @param       string $func The function form of the item slug.
		 * @return      void
		 */
		public function __construct( $slug, $func ) {
			$this->slug = $slug;
			$this->func = $func;
		}


		/**
		 * Get a CSS-valid unit value from a string
		 *
		 * @access      public
		 * @since       1.0.0
		 * @param       string $input The value to convert.
		 * @param       string $default A default value.
		 * @param       string $format Whether to return a formatted string or the composit array.
		 * @return      mixed string|array $output The valid CSS value.
		 */
		public function get_css_dimension( $input, $default = 'auto', $format = 'string' ) {

			$valid_units = $this->get_css_dimension_valid_units();
			$passthrough = $this->get_css_dimension_passthrough();

			$output = array(
				'value' => '',
				'unit'  => '',
			);

			if ( in_array( $input, $passthrough ) ) {
				$output['value'] = $input;
			} else {
				if ( '%' === substr( $input, -1 ) ) {
					$output['value'] = substr( $input, 0, -1 );
					$output['unit']  = '%';
				} elseif ( in_array( substr( $input, -2 ), $valid_units ) ) {
					$output['value'] = substr( $input, 0, -2 );
					$output['unit']  = substr( $input, -2 );
				} elseif ( is_numeric( $input ) ) {
					$output['value'] = $input;
					$output['unit']  = 'px';
				} else {
					$output = apppresser_shops_get_css_value( $default );
				}
			}

			if ( 'array' !== $format ) {
				$output = $output['value'] . $output['unit'];
			}

			return apply_filters( $this->func . '_get_css_value', $output );
		}


		/**
		 * Get an array of valid CSS unit types
		 *
		 * Technically the default list is incomplete. The current CSS spec
		 * allows two types of units: relative and absolute. Absolute units
		 * currently supported by the spec are as follows:
		 *
		 * - em
		 * - ex
		 * - ch
		 * - rem
		 * - vw
		 * - vh
		 * - vmin
		 * - vmax
		 * - %
		 *
		 * Relative units are the "preferred" unit type as they are based
		 * on size relative to something else. Absolute units are generally
		 * not recommended for on-screen display because screen size can
		 * have a direct effect on how things are displayed on-screen.
		 * The current CSS spec allows for the following absolute units,
		 * though only pixels are included in our whitelist:
		 *
		 * - cm
		 * - mm
		 * - in
		 * - px
		 * - pt
		 * - pc
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      array $types The valid CSS unit types.
		 */
		public function get_css_dimension_valid_units() {
			$valid_units = apply_filters(
				$this->func . '_css_dimension_valid_units',
				array(
					'px',
					'em',
					'ex',
					'ch',
					'rem',
					'vw',
					'vh',
					'vmin',
					'vmax',
				)
			);

			return $valid_units;
		}


		/**
		 * Get non-numeric values allowed by the dimension helper
		 *
		 * Generally, this should only be
		 * the three values included by default, but it is filterable
		 * so the developers can add support for things like template
		 * tags.
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      array $passthrough The valid passthrough values.
		 */
		public function get_css_dimension_passthrough() {
			$passthrough = apply_filters(
				$this->func . '_css_dimension_passthrough',
				array(
					'auto',
					'inherit',
					'initial',
				)
			);

			return $passthrough;
		}


		/**
		 * Check if the displayed page is a login page.
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      bool $is_login Whether or not this is a login page.
		 */
		public function is_login() {
			global $pagenow;

			$abspath_sane = str_replace(
				array( '\\', '/' ),
				DIRECTORY_SEPARATOR,
				ABSPATH
			);

			$included = get_included_files();
			$is_login = false;

			$login_pages = apply_filters(
				$this->func . '_login_pages',
				array(
					'wp-login.php',
					'wp-register.php',
				)
			);

			foreach ( $login_pages as $login_page ) {
				if ( in_array( $abspath_sane . $login_page, $included ) ) {
					$is_login = true;
				}

				if ( isset( $pagenow ) && $login_page === $pagenow ) {
					$is_login = true;
				}
			}

			return $this->func . apply_filters( '_is_login', $is_login, $login_pages );
		}
	}
}
