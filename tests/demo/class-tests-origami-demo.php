<?php
/**
 * Core unit tests
 *
 * @package     WidgitLabs\Origami\Tests\Demo
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Core unit tests
 *
 * @since       1.0.0
 */
class Tests_Origami_Demo extends Tests_Origami_Loader {
	// This is not a core file and we can't control non-WordPress code.
	// phpcs:disable WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 */
	public function setUp() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
		parent::setUp();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
		parent::tearDown();
	}


	/**
	 * Test instance
	 *
	 * @access      public
	 * @since       2.0.0
	 * @return      void
	 * @covers      Origami_Demo::instance
	 * @covers      Origami_Demo::setup_constants
	 * @uses        ::origami_demo
	 */
	public function test_instance() {
		$this->assertClassHasStaticAttribute( 'instance', 'Origami_Demo' );
		$this->assertIsCallable( 'origami_demo' );

		$plugin_url = trailingslashit( str_replace( 'tests/demo/', '', plugin_dir_url( __FILE__ ) ) );
		$this->assertSame( ORIGAMI_URL, $plugin_url );

		$plugin_path  = str_replace( 'tests/demo/', '', plugin_dir_path( __FILE__ ) );
		$plugin_path  = trailingslashit( substr( $plugin_path, 0, -1 ) );
		$compare_path = trailingslashit( substr( ORIGAMI_DIR, 0, -1 ) );
		$this->assertSame( $plugin_path, $compare_path );

		$this->assertFileExists( $plugin_path . 'CHANGELOG.md' );
		$this->assertFileExists( $plugin_path . 'class-origami-loader.php' );
		$this->assertFileExists( $plugin_path . 'class-origami.php' );
		$this->assertFileExists( $plugin_path . 'CONTRIBUTING.md' );
		$this->assertFileExists( $plugin_path . 'license.txt' );
		$this->assertFileExists( $plugin_path . 'README.md' );
		$this->assertFileExists( $plugin_path . 'assets/css/admin.css' );
		$this->assertFileExists( $plugin_path . 'assets/css/admin.min.css' );
		$this->assertFileExists( $plugin_path . 'assets/css/jquery-ui-classic.min.css' );
		$this->assertFileExists( $plugin_path . 'assets/css/jquery-ui-fresh.min.css' );
		$this->assertFileExists( $plugin_path . 'assets/js/admin.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/admin.min.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2/js/select2.full.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2/js/select2.full.min.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2/js/select2.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2/js/select2.min.js' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2/css/select2.css' );
		$this->assertFileExists( $plugin_path . 'assets/js/select2/css/select2.min.css' );
		$this->assertFileExists( $plugin_path . 'demo/class-origami-demo.php' );
		$this->assertFileExists( $plugin_path . 'demo/register-settings.php' );
		$this->assertFileExists( $plugin_path . 'languages/README.md' );
		$this->assertFileExists( $plugin_path . 'modules/licensing/class-origami-licensing.php' );
		$this->assertFileExists( $plugin_path . 'modules/licensing/class-origami-plugin-updater.php' );
		$this->assertFileExists( $plugin_path . 'modules/sysinfo/class-origami-browser.php' );
		$this->assertFileExists( $plugin_path . 'modules/sysinfo/class-origami-sysinfo.php' );
	}
}
