<?php
/**
 * Loader unit tests
 *
 * @package     WidgitLabs\Origami\Tests\Loader
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Loader unit tests
 *
 * @since       1.0.0
 *
 * @uses ::origami_demo
 */
class Tests_Origami_Loader extends WP_UnitTestCase {
	// This is not a core file and we can't control non-WordPress code.
	// phpcs:disable WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = origami_demo();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod.Found
		parent::tearDown();
	}


	/**
	 * Test origami_get_demo_mode_status
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 * @covers      ::origami_get_demo_mode_status
	 * @uses        Origami_Demo::instance
	 * @uses        ::origami_demo
	 */
	public function test_origami_get_demo_mode_status() {
		// TODO: Make sure ALL tests support network.
		$status = (bool) get_option( 'origami_demo_mode' );
		if ( is_network_admin() ) {
			$status = (bool) get_network_option( null, 'origami_demo_mode' );
		}

		$this->assertSame( $status, origami_get_demo_mode_status() );
	}


	/**
	 * Test origami_toggle_demo_mode_status
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 * @covers      ::origami_toggle_demo_mode_status
	 * @uses        Origami_Demo::instance
	 * @uses        ::origami_demo
	 * @uses        ::origami_get_demo_mode_status
	 */
	public function test_origami_toggle_demo_mode_status() {
		$this->assertSame( true, (bool) origami_get_demo_mode_status() );
		origami_toggle_demo_mode_status();
		$this->assertSame( false, (bool) origami_get_demo_mode_status() );
		origami_toggle_demo_mode_status();
	}


	/**
	 * Test origami_demo
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 * @covers      ::origami_demo
	 * @uses        Origami::get_version
	 * @uses        Origami_Demo::instance
	 */
	public function test_origami_demo() {
		$settings_object = origami_demo();

		$this->assertEquals( '1.0.0', $settings_object->settings->get_version() );
	}
}
